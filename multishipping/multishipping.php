<?php
/*
  
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * or OpenGPL v3 license (GNU Public License V3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * or
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @category   Balticode
 * @package    Balticode_Multisiuntos
 * @copyright  Copyright (c) 2015 UAB Balticode (http://balticode.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @license    http://www.gnu.org/licenses/gpl-3.0.txt  GNU Public License V3.0
 * @author     Kęstutis Kaleckas
 * 

 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(_PS_MODULE_DIR_.'multishipping/models/array2xml.php');
require_once(_PS_MODULE_DIR_.'multishipping/models/cretedownload.php');
require_once(_PS_MODULE_DIR_.'multishipping/ConfigSourceData.php');
if (!class_exists('AdminOrderBulkAction', false)) {
    require_once(_PS_MODULE_DIR_.'multishipping/controllers/admin/AdminOrdersController.php');
}


class multishipping extends Module
{
    const CONST_PREFIX = 'MULTISHIPPING_';
    public  $id_carrier;
    private $_postErrors = array();
    private $_html = '';
    public static $_moduleName = 'multishipping';

    private $express_available = array(
        'TNT',
        'TNT_LT',
        'TNT_LV',
        'TNT_EE',
        'DHL',
        'DHL_LT',
        'DHL_LV',
        'DHL_EE',
        'UPS_LT',
        'UPS_LV',
        'UPS_EE',
    );
    private $cod_methods = array('cashondelivery');
    private $shipping_methods_to_get_parcel_stores = array("balticode_dpd_parcelstore","post24lithuania");

    private $mimeTypes = array(
        'pdf' => 'application/pdf',
        'txt' => 'text/plain',
        'html' => 'text/html',
        'exe' => 'application/octet-stream',
        'zip' => 'application/zip',
        'doc' => 'application/msword',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'gif' => 'image/gif',
        'png' => 'image/png',
        'jpeg' => 'image/jpg',
        'jpg' => 'image/jpg',
        'php' => 'text/plain'
    );


    /* Constructor */
    public function __construct()
    {
        $this->name = 'multishipping';
        $this->version = '1.0.2';
        $this->tab = 'export';
        $this->author = 'Balticode.com';
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6.9');
        $this->need_instance = 0;
        parent::__construct();
        $this->displayName = $this->l('Multisiuntos');
        $this->description = $this->l('XML shipping methods generator.');
        $this->confirmUninstall = $this->l('Are you sure you want to delete this module?');
        $this->_errors = array();
    }

    /* Installer */
    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('actionAdminControllerSetMedia'))
            return false;

        $array_to_replace[] = array(
            "search" => 'parent::__construct();',
            "replace" => 'if(Configuration::get(\'MULTISHIPPING_ENABLED\'))
         {
            $this->bulk_actions[\'GenerateMultishippingXML\'] = array(\'text\' => $this->l(\'Generuoti XML\'));
         }
         ',
            "action" => 'before',
        );
        $array_to_replace[] = array(
            "search" => 'public function initToolbar()',
            "replace" => 'public function processBulkGenerateMultishippingXML()
    {
        $order_ids = Tools::getValue(\'orderBox\');
        $multishipping = Module::getInstanceByName(\'multishipping\');
        $multishipping->createXML($order_ids);
    }
    ',
            "action" => 'before',
        );
        if(!$this->makeOverride($array_to_replace)) return false;
        return true;
    }


    private function makeOverride($rows)
    {
        $config = serialize($rows);
        Configuration::updateValue(self::CONST_PREFIX . strtoupper("OVERWRITE"), $config); //save who we change
        $test = new AdminOrderBulkAction();
        foreach ($rows as $row) {
            $test->addToReplace($row['search'],$row['replace'], $row['action']);
        }
        return $test->run();
    }

    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $this->_postProcess();
        }
        $this->context->smarty->assign(
                array(
                    'moduleName' => $this->name,
                    'displayName' => $this->displayName,
                    'action' => Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']),
                    'method' => 'POST',
                    'enabled' => Configuration::get(self::CONST_PREFIX . 'ENABLED'),
                    'comment' => Configuration::get(self::CONST_PREFIX . 'COMMENT'),
                    'all_carriers' => $this->getCarriersList(),
                    'settings' => $this->flipArrayList(unserialize(Configuration::get(self::CONST_PREFIX . 'SETTINGS'))),
                    'source_data' => Multisiuntos_ConfigSourceData::toOptionArray(),
                )
            ); 
        return $this->display(__FILE__, 'views/templates/admin/configure.tpl');
    }


    /* Save changes of settings */
    private function _postProcess($skip = array('tab','btnSubmit')){
        $data = $_POST;
        foreach ($data as $name => $value) {
            if(in_array($name, $skip)) continue; //Skip not infomative fields
            if(is_array($value)) $value = serialize(Tools::getValue($name)); else $value = Tools::getValue($name); //if array so serilizse it
            Configuration::updateValue(self::CONST_PREFIX . strtoupper($name), $value);
        }
    }

    private function getCarriersList($id_lang = null)
    {
        if(empty($id_lang)) $id_lang = $this->context->cookie->id_lang;
        return Carrier::getCarriers($id_lang,false,false,false,null,5);
    }

    // /* Uninstaller */
    public function uninstall()
    {
         if (!parent::uninstall())
            return false;
        
        $rows = unserialize( Configuration::get(self::CONST_PREFIX . 'OVERWRITE') );
        foreach ($rows as $row) {
            $array_to_replace[] = array(
                "search" => $row['replace'],
                "replace" => '',
                "action" => 'replace',
            );
        }

        if(!$this->makeOverride($array_to_replace)) return false;

        $settings = array(
                    self::CONST_PREFIX . 'ENABLED',
                    self::CONST_PREFIX . 'SETTINGS',
                    self::CONST_PREFIX . 'COMMENT',
                    self::CONST_PREFIX . 'OVERWRITE',
                );
        foreach ($settings as $setting) {
            Configuration::deleteByName($setting);
        }

        return true;
    }

    public function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang)
            $tab->name[$lang['id_lang']] = $name;
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;
        return $tab->add();
    }

    /**
     * <p>For adding CSS,JS scripts in backoffice</p>
     */
    public function hookActionAdminControllerSetMedia() {
        if(isset($_GET['configure'])){
            if($_GET['configure'] == self::$_moduleName){
                $this->context->controller->addCSS($this->_path . 'css/' . self::$_moduleName . '.css', 'all');
                $this->context->controller->addJS($this->_path . 'js/' . self::$_moduleName . '.js');
            }
        }
    }

    public function createXML($orderIds)
    {

        if(!isset($orderIds)) return false; //If something wrong
        //$data = Mage::getModel('multisiuntos/data');
        //$helper = Mage::helper("multisiuntos/data");

        $orders_array = array(
            '@attributes' => array(
                'version' => '1'
            ),
            'shipment' => array(
            )
        );
        foreach ($orderIds as $orderId) {
            $order = new Order($orderId);
            $order_data = get_object_vars($order);
            $address = new Address((int)$order->id_address_delivery);
            $customer= new Customer((int)$order->id_customer);
            $cart = new Cart((int)$order->id_cart);
            $carrier = new Carrier((int)$cart->id_carrier);

            $orders_array['shipment'][] = array(
            "reference" => $order_data['id'], //Orderio Id
            "weight" => (float)$this->getDecimal($cart->getTotalWeight(),8,3), //(float)$data->getDecimal($order_data['weight'],8,3), //Svoris kilogramais
            "remark" => array(), //$data->getComment($order), //Papildoma informacija, kuri eina ant lipduko
            "additional_information" => null, //Papildoma informacija
            //"number_of_parcels" => $this->getCartTotalQty($order->id_cart), //(int)$order['total_qty_ordered'], //Pakuociu skaicius per uzsakyma
            "number_of_parcels" => 1,
            "courier_identifier" => ((count($this->getIdentificatorData($order->id_carrier,'shipping_identifier')) != 0 )?$this->getIdentificatorData($order->id_carrier,'shipping_identifier'):$carrier->name)  , //$helper->getCourierIdentifierByShippingMethod($order_data['shipping_method']), //Kurjerio identifikatorius
            "receiver" => array( //Gavejo info
                    "name" => $address->firstname.' '.$address->lastname, //$shipping_data['firstname'], //Vardas
                    "street" => $address->address1, //$shipping_data['street'], //Gatve
                    "postal_code" => $address->postcode, //$shipping_data['postcode'], //Pasto kodas
                    "city" => $address->city, //$shipping_data['city'], //Miestas
                    "phone" => $address->phone_mobile, //$shipping_data['telephone'], //Telefono numeris
                    "email" => $customer->email, //$order_data['customer_email'], //Elektroninio pasto adresas
                    "parcel_terminal_identifier" => $this->getParcelStoreId($orderId), //$helper->getParcelStoreId($order), //Siuntu tasko ID
                    "country_code" => country::getIsoById($address->id_country), //$shipping_data['country_id'] //Salies kodas
                ),
            "services" => array( //Vezejo info
                "cash_on_delivery" => $this->getPayment($orderId), //$helper->getPayment($order), //Gauname COD duomenis
                "express_delivery" => ((count($this->getIdentificatorData($order->id_carrier,'express')) != 0 )?$this->getIdentificatorData($order->id_carrier,'express'):0), //(bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'express' ), //Ar tai Express pristatymas?
                "self_service" => ((count($this->getIdentificatorData($order->id_carrier,'pickup')) != 0 )?$this->getIdentificatorData($order->id_carrier,'pickup'):0), //(bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'self' ), //Ar reikalingas pakrovimas
                "loading_service" => ((count($this->getIdentificatorData($order->id_carrier,'loading')) != 0 )?$this->getIdentificatorData($order->id_carrier,'loading'):0), //(bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'loading' ), //Pakrovimo servisas
                "saturday_delivery" => ((count($this->getIdentificatorData($order->id_carrier,'saturday')) != 0 )?$this->getIdentificatorData($order->id_carrier,'saturday'):0), //(bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'saturday' ), //Savaitgalinis vezimas
                "insurance" => ((count($this->getIdentificatorData($order->id_carrier,'insurance')) != 0 )?$this->getIdentificatorData($order->id_carrier,'insurance'):0), //(bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'insurance' ), //Draudimas
                "proof_of_delivery" => ((count($this->getIdentificatorData($order->id_carrier,'proof')) != 0 )?$this->getIdentificatorData($order->id_carrier,'proof'):0), //(bool)$helper->getCourierAttributeByShippingMethod($order_data['shipping_method'], 'proof' ) //Pristatymo patvirinimas
                )
            );
        }

        $xml = array2xml::createXML('shipments', $orders_array);
        $this->download("couriers.xml", $xml->saveXML(), 'xml');
    }
    
    public function getPayment($id_order){
        $order = new Order($id_order);
        $currency = new Currency($order->id_currency);
        $COD_array = array();
        if(in_array($order->module,$this->cod_methods)){ //if COD grab info
            $COD_array = array("value" => $this->getDecimal($order->total_paid,8,2), //Siuntinio suma
                     "reference" => null, //Klienu ir sistemos vidinis COD suma
                     "currency" => $currency->iso_code //Valiuta
                     );
        }
        return $COD_array;
    }

    public function getDecimal($number, $precision, $scale){
        return number_format($number,$scale,".","");
    }

    public function getCartTotalQty($id_cart)
    {
        $cart = new Cart($id_cart);
        $qty = 0;
        foreach ($cart->getProducts() as $product) {
            $qty += $product['quantity']; 
        }
        return $qty;
    }

    private function getExpressAvailable($id_carrier)
    {
        $carrier = new Carrier($id_carrier);
        if(in_array($carrier->name, $this->express_available)){
            return true;
        }
        return false;
    }

    public function getIdentificatorData($id_carrier,$field = null)
    {
        $data = (unserialize(Configuration::get(self::CONST_PREFIX . 'SETTINGS'))); //get all settings
        if(!in_array($id_carrier, $data['shipping_methods'])) return array(); // test this carrier has settings?
        $line = array_flip($data['shipping_methods']); // get line key where is correct data
        $data = $this->flipArrayList($data);
        if($field == 'express' && !$this->getExpressAvailable($id_carrier)){ //If atrribute is express, nead to test it is TNT UPS or DHL courier
            return 0;
        }
        if(!empty($field)) return $data[$line[$id_carrier]][$field]; // return selected field
        return $data[$line[$id_carrier]]; //return all block of settings
    }

    private function download($file_name, $content, $file_type = 'application/octet-stream')
    {
        if(in_array($file_type, array_keys($this->mimeTypes))) {
            $file_type = $this->mimeTypes[$file_type];
        }
        $cretedownload = new cretedownload($file_name, $file_type);
        $cretedownload->render($content);
    }


    /**
     * <p>Attempts to decode extra data stored within order commetns and return it as array.</p>
     * @param OrderCore $order
     * @return array
     */
    public function getDataFromOrder($order) {
        return $this->_baseInstance->_getHelperModule()->getDataFromOrder($order, self::ORDER_COMMENT_START_PREFIX);
    }

    /**
     * Flip DualDimensional Array
     *
     * @param array
     * @return  array
     */
    public function flipArrayList($list)
    {
        $new_array = array();
        if($list && count($list)){
            $options = array_keys($list); // get all keys c_name, name, c_post and etc
            $values_count = array_keys($list[$options[0]]); //get how much records availible
            foreach ($values_count as $row_nr => $row) { //get rows
                foreach ($options as $key => $option) {
                    $new_array[$row][$option] = $list[$option][$row];
                }
            }
        }
        return $new_array;
    }

    public function getParcelStoreId($orderId){
        $order = new Order($orderId);
        $cart = new Cart((int)$order->id_cart);
        $carrier = new Carrier((int)$cart->id_carrier);
        if(!in_array($carrier->name, $this->shipping_methods_to_get_parcel_stores)) return array(); //can I got a a parcel store
        $order_data = get_object_vars($order);
        $address = new Address((int)$order->id_address_delivery);
        $customer= new Customer((int)$order->id_customer);
        
        switch($carrier->name){
            case ("balticode_dpd_parcelstore"):
                $patch_to_file = _PS_MODULE_DIR_.'balticode_postoffice/balticode_postoffice.php';
                if(file_exists($patch_to_file)){
                    require_once($patch_to_file);
                    $temp = new Balticode_Postoffice();
                    $office = array_values( $temp->getOfficesFromCart((int)$order->id_cart) );
                    return (isset($office[0]['remote_place_id'])?$office[0]['remote_place_id']:0);
                }
            break;
            case ("post24lithuania"):
                $patch_to_file = _PS_MODULE_DIR_.'eabi_postoffice/eabi_postoffice.php';
                if(file_exists($patch_to_file)){
                    require_once($patch_to_file);
                    $temp = new Eabi_Postoffice();
                    $office = array_values( $temp->getOfficesFromCart((int)$order->id_cart) );
                    return (isset($office[0]['remote_place_id'])?$office[0]['remote_place_id']:0);
                }
            break;
        }
        return array();
    }
}