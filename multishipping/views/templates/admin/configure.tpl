<div id="multishipping">
	<img src="../modules/{$moduleName}/logo.png" class="multishipping_logo_img"><h2 class="multishipping_inline">{$displayName}</h2>
	<form action="{$action}" method="{$method}">
		<fieldset>
			<legend><img src="../img/admin/contact.gif" alt="" />{l s='Configuration details' mod='multishipping'}</legend>
				<label for="company">{l s='Enable' mod='multishipping'} :</label>
				<div class="margin-form">
					<img src="../img/admin/enabled.gif" alt="Yes" title="Yes">
					<input name="enabled" id="enable_on" 
					{if $enabled eq '1' }
						checked="checked" 
					{/if}
					value="1" type="radio">
					<label class="t" for="enable_on">{l s='Yes' mod='multishipping'}</label>
					<img src="../img/admin/disabled.gif" alt="No" title="No" style="margin-left: 10px;">
					<input name="enabled" id="enable_off"
					{if $enabled eq '0' }
						checked="checked" 
					{/if}
					 value="0" type="radio">
					<label class="t" for="enable_off">{l s='No' mod='multishipping'}</label>
				</div>
				<label for="service_username">{l s='Multisiuntos' mod='multishipping'} :</label>
				<div class="grid" id="grid_formFieldId ">
				    <table cellpadding="0" cellspacing="0" class="border">
				        <thead>
				            <tr class="headings" id="heading">
				                <th>{l s='Shipping Methods' mod='multishipping'}</th>
				                <th>{l s='Courier identifier' mod='multishipping'}</th>
				                <th>{l s='Express' mod='multishipping'}</th>
				                <th>{l s='Pickup is required' mod='multishipping'}</th>
				                <th>{l s='Loading is required' mod='multishipping'}</th>
				                <th>{l s='Saturday' mod='multishipping'}</th>
				                <th>{l s='Insurance' mod='multishipping'}</th>
				                <th>{l s='Proof' mod='multishipping'}</th>
				                <th></th>
				            </tr>
				        </thead>
				        <tbody id="multishipping_body">
			{if $settings}
				{foreach from=$settings key=id item=line}
					<tr class="headings">
			            <td>
			            <select name="settings[shipping_methods][]" class="shipping_methods">
							{foreach from=$all_carriers key=id item=carrier}
									<option value="{$carrier.id_carrier}" {if $line.shipping_methods eq $carrier.id_carrier} selected="selected" {/if}>{$carrier.name}</option>
							{/foreach}
						</select>
			            </td>
			            <td>
			            <select name="settings[shipping_identifier][]" class="shipping_identifier">
							{foreach from=$source_data key=id item=carrier}
									<option value="{$carrier.value}" {if $line.shipping_identifier eq $carrier.value} selected="selected" {/if} >{$carrier.label}</option>
							{/foreach}
						</select>
			            </td>
			            <td>
				            <select name="settings[express][]">
				            	<option value="0" {if $line.express eq "0"} selected="selected" {/if}>No</option>
				            	<option value="1" {if $line.express eq "1"} selected="selected" {/if}>Yes</option>
				            </select>
			            </td>
			            <td>
				            <select name="settings[pickup][]">
				            	<option value="0"{if $line.pickup eq "0"} selected="selected" {/if}>No</option>
				            	<option value="1"{if $line.pickup eq "1"} selected="selected" {/if}>Yes</option>
				            </select>
			            </td>
			            <td>
				            <select name="settings[loading][]">
				            	<option value="0"{if $line.loading eq "0"} selected="selected" {/if}>No</option>
				            	<option value="1"{if $line.loading eq "1"} selected="selected" {/if}>Yes</option>
				            </select>
			            </td>
			            <td>
			            	<select name="settings[saturday][]">
				            	<option value="0"{if $line.saturday eq "0"} selected="selected" {/if}>No</option>
				            	<option value="1"{if $line.saturday eq "1"} selected="selected" {/if}>Yes</option>
				            </select>
			            <td>
				            <select name="settings[insurance][]">
				            	<option value="0"{if $line.insurance eq "0"} selected="selected" {/if}>No</option>
				            	<option value="1"{if $line.insurance eq "1"} selected="selected" {/if}>Yes</option>
				            </select>
			            </td>
			            <td>
				            <select name="settings[proof][]">
				            	<option value="0"{if $line.proof eq "0"} selected="selected" {/if}>No</option>
				            	<option value="1"{if $line.proof eq "1"} selected="selected" {/if}>Yes</option>
				            </select>
			            </td>
			            <td>
			            	<button onclick="multishipping.event.remove(this);" class="" type="button"><span>{l s='Delete' mod='multishipping'}</span></button>
			            </td>
			        </tr>
				{/foreach}
			{/if}
				        </tbody>
				        <tfoot>
				            <tr id="addRow_formFieldId ">
				                <td colspan="8"></td>
				                <td >
				                    <button style="" onclick="multishipping.event.addListener('#multishipping_template', '#multishipping_body');" class="" type="button" id="addToEndBtn">
				                        <span>{l s='Add combination' mod='multishipping'}</span>
				                    </button>
				                </td>
				            </tr>
				        </tfoot>
				    </table>
				</div>
				<!--
				<label for="company">{l s='Add last order comment to XML' mod='multishipping'} :</label>
				<div class="margin-form">
					<img src="../img/admin/enabled.gif" alt="Yes" title="Yes">
					<input name="comment" id="enable_on" 
					{if $comment eq '1' }
						checked="checked" 
					{/if}
					value="1" type="radio">
					<label class="t" for="enable_on">{l s='Yes' mod='multishipping'}</label>
					<img src="../img/admin/disabled.gif" alt="No" title="No" style="margin-left: 10px;">
					<input name="comment" id="enable_off"
					{if $comment eq '0' }
						checked="checked" 
					{/if}
					 value="0" type="radio">
					<label class="t" for="enable_off">{l s='No' mod='multishipping'}</label>
				</div>
				-->
				<div class="margin-form">
					<input class="button" name="btnSubmit" value="{l s='Update settings' mod='multishipping'}" type="submit" />
				</div>
		</fieldset>
	</form>
    <script type="text/html" id="multishipping_template" class="hide">
        <tr class="headings" id="heading">
            <td>
            <select name="settings[shipping_methods][]" class="shipping_methods">
				{foreach from=$all_carriers key=id item=carrier}
						<option value="{$carrier.id_carrier}">{$carrier.name}</option>
				{/foreach}
			</select>
            </td>
            <td>
            <select name="settings[shipping_identifier][]" class="shipping_identifier">
				{foreach from=$source_data key=id item=carrier}
						<option value="{$carrier.value}">{$carrier.label}</option>
				{/foreach}
			</select>
            </td>
            <td>
	            <select name="settings[express][]">
	            	<option value="0">No</option>
	            	<option value="1">Yes</option>
	            </select>
            </td>
            <td>
	            <select name="settings[pickup][]">
	            	<option value="0">No</option>
	            	<option value="1">Yes</option>
	            </select>
            </td>
            <td>
	            <select name="settings[loading][]">
	            	<option value="0">No</option>
	            	<option value="1">Yes</option>
	            </select>
            </td>
            <td>
            	<select name="settings[saturday][]">
	            	<option value="0">No</option>
	            	<option value="1">Yes</option>
	            </select>
            <td>
	            <select name="settings[insurance][]">
	            	<option value="0">No</option>
	            	<option value="1">Yes</option>
	            </select>
            </td>
            <td>
	            <select name="settings[proof][]">
	            	<option value="0">No</option>
	            	<option value="1">Yes</option>
	            </select>
            </td>
            <td>
            	<button onclick="multishipping.event.remove(this);" class="" type="button"><span>{l s='Delete' mod='multishipping'}</span></button>
            </td>
        </tr>
	</script>
</div>