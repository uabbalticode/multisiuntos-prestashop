<?php
class Multisiuntos_ConfigSourceData
{
    /**
     * Options getter
     *
     * @return array
     */
    static public function toOptionArray()
    {
        return array(
			array('value' => 'OMNIVA_LT', 'label' => 'OMNIVA_LT'),
			array('value' => 'OMNIVA_LV', 'label' => 'OMNIVA_LV'),
			array('value' => 'OMNIVA_EE', 'label' => 'OMNIVA_EE'),
			array('value' => 'LP_EXPRESS', 'label' => 'LP_EXPRESS'),
			array('value' => 'VENIPAK', 'label' => 'VENIPAK'),
			array('value' => 'DPD_LT', 'label' => 'DPD_LT'),
			array('value' => 'DPD_LV', 'label' => 'DPD_LV'),
			array('value' => 'DPD_EE', 'label' => 'DPD_EE'),
			array('value' => 'UPS', 'label' => 'UPS'),
			array('value' => 'UPS_LT', 'label' => 'UPS_LT'),
			array('value' => 'UPS_LV', 'label' => 'UPS_LV'),
			array('value' => 'UPS_EE', 'label' => 'UPS_EE'),
			array('value' => 'TNT', 'label' => 'TNT'),
			array('value' => 'TNT_LT', 'label' => 'TNT_LT'),
			array('value' => 'TNT_LV', 'label' => 'TNT_LV'),
			array('value' => 'TNT_EE', 'label' => 'TNT_EE'),
			array('value' => 'DHL', 'label' => 'DHL'),
			array('value' => 'DHL_LT', 'label' => 'DHL_LT'),
			array('value' => 'DHL_LV', 'label' => 'DHL_LV'),
			array('value' => 'DHL_EE', 'label' => 'DHL_EE'),
			array('value' => 'LT_POST', 'label' => 'LT_POST'),
			array('value' => 'LV_POST', 'label' => 'LV_POST'),
			array('value' => 'ITELLA', 'label' => 'ITELLA'),
			array('value' => 'SMARTBUS', 'label' => 'SMARTBUS'),
        );
    }

	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	static public function toArray()
	{
	    return array(
			'OMNIVA_LT' => 'OMNIVA_LT',
			'OMNIVA_LV' => 'OMNIVA_LV',
			'OMNIVA_EE' => 'OMNIVA_EE',
			'LP_EXPRESS' => 'LP_EXPRESS',
			'VENIPAK' => 'VENIPAK',
			'DPD_LT' => 'DPD_LT',
			'DPD_LV' => 'DPD_LV',
			'DPD_EE' => 'DPD_EE',
			'UPS' => 'UPS',
			'UPS_LT' => 'UPS_LT',
			'UPS_LV' => 'UPS_LV',
			'UPS_EE' => 'UPS_EE',
			'TNT' => 'TNT',
			'TNT_LT' => 'TNT_LT',
			'TNT_LV' => 'TNT_LV',
			'TNT_EE' => 'TNT_EE',
			'DHL' => 'DHL',
			'DHL_LT' => 'DHL_LT',
			'DHL_LV' => 'DHL_LV',
			'DHL_EE' => 'DHL_EE',
			'LT_POST' => 'LT_POST',
			'LV_POST' => 'LV_POST',
			'ITELLA' => 'ITELLA',
			'SMARTBUS' => 'SMARTBUS',
	    );
	}
}
?>