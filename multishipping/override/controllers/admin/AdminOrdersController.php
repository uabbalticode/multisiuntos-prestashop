<?php

class AdminOrdersController extends AdminOrdersControllerCore
{
    public function __construct()
    {
        parent::__construct();
        if(Configuration::get('MULTISHIPPING_ENABLED'))
        {
           $this->bulk_actions['GenerateMultishippingXML'] = array('text' => $this->l('Generuoti XML'));
        }       
    }

    public function processBulkGenerateMultishippingXML()
    {
        $order_ids = Tools::getValue('orderBox');
        $multishipping = Module::getInstanceByName('multishipping');
        $multishipping->createXML($order_ids);
    }
}

?>